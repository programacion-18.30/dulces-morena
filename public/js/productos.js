//  LA API FETCH PROPORCIONA UNA INTERFAZ PARA RECUPERAR RECURSOS (INCLUSO, A TRAVÉS DE LA RED)
let traeProductos = () => {
    fetch('/productos/',  {method: 'GET'})
        .then(response => response.json())
        .then(data => armaTemplate(data));

    function armaTemplate(productos) {
        // console.log(productos);
        let template = "";
        productos.forEach(producto => {
            let precioFormateado = new Intl.NumberFormat('es-AR').format(producto.precio);
            template += 
            `<article>
                 <div class="sumo" onclick="sumoItem(${producto.id},'${producto.descripcion}','${producto.imagen}',${producto.precio})"><img src="mas.png"></div>
                <h3 class ="descripcion">${producto.descripcion}</h3>
                <img src="${producto.imagen}" class ="imagen">
                 <p>Precio $${precioFormateado}</p></article>`
        });
        
        document.querySelector("#producto").innerHTML = template;
    }
}

traeProductos();
let carrito = [];
let contador = 0;
let productoEstaba;

function sumoItem(id, desc, imagen, precio) {
    console.log("Entro en sumo");
    productoEstaba = false;
    contador++;
    //ARMA EL OBJETO ITEM
    let item = {
        'id': id,
        'descripcion': desc,
        'imagen': imagen,
        'precio': precio,
        'cantidad': 0,
    }
    console.log("item:", item);

    document.querySelector('#contador').innerHTML = contador;

    for (let i = 0; i < carrito.length; i++) {
        console.log("Dentro del for, carrito:", carrito);
        console.log("Entré en el for: i: ", i, " - item.producto: ", item.id, " - carrito[i].producto: ", carrito[i].producto);
        if (item.id === carrito[i].id) {
            productoEstaba = true;
            carrito[i].cantidad++;
        }

    }
    if (!productoEstaba) {
        console.log("Carrito desde inserción: ", carrito);
        item.cantidad = 1;
        let elem = carrito.push(item);
    }
    console.log("Muestro el carrito:", carrito);
    localStorage.setItem("carritoDulceMorena", JSON.stringify(carrito));

}
function listado() {
    //MUESTRA LA VENTANA MODAL
    modal.style.opacity = 1;
    modal.style.visibility = "visible";
    armaModal();
}
let totalCarrito;
//Funcion derivada de listado
function armaModal() {
    totalCarrito = 0;
    let html = "<table><thead><th>Imagen </th><th>ID</th><th>Descripción </th><th>Precio </th><th>Cantidad </th></thead><tbody>";
    for (let i = 0; i < carrito.length; i++) {
        console.log("Mostrando Carrito: ",carrito[i]);
        let producto = carrito[i];
        let producto_importe = producto.cantidad * producto.precio;
        totalCarrito = totalCarrito + producto_importe;
        console.log("Mostrando objeto producto: ",producto.imagen," - ",producto.descripcion, " - ", producto.precio, " - ", producto.cantidad);
        html += `<tr>
        <td><img src="${producto.imagen}"></td>
        <td>${producto.id}</td>
        <td>${producto.descripcion}</td>
        <td>${new Intl.NumberFormat('es-AR').format(producto.precio)}</td>
        <td><div class="cuadradito" onclick="sumo(${producto.id})">+</div>
        <div id="canti${producto.id}">${producto.cantidad}</div>
        <div class="cuadradito" onclick="resto(${producto.id})">-</div></td>
        <td><div id="importe${producto.id}">${new Intl.NumberFormat('es-AR').format(producto_importe)}</td></tr>`;
    }
    html += `<td colspan="5" style="text-align: right; padding: 10px; font-weight: bold">Total compra: </td><td><div id="tc" style="font-weight: bold">${new Intl.NumberFormat('es-AR').format(totalCarrito)}</div></td>`
    html += "</tbody></table>";
    document.querySelector(".tabla").innerHTML = html;
}

function sumo(id) {
    console.log("estoy en sumo");
    //tomo el producto del carrito segun el id y lo asigno a producto
    let producto = carrito.find(element => element.id = id);
    console.log("Producto en sumo: ",producto);
    //determino la posicion donde encontro el id
    let posicion = carrito.indexOf(producto);
    console.log(posicion);
    producto.cantidad++;
    //reemplazo el producto en el carrito, en la posicion correspondiente
    carrito.splice(posicion, 1, producto);
    console.log("Carrito en la fila modificada: ",carrito[posicion]);
    console.log("Carrito completo despues de sumar: ",carrito);//donde vemos la suma realizada
    //muestro la cantidad nueva por pantalla
    document.querySelector(`#canti${producto.id}`).innerHTML = producto.cantidad;
    let producto_importe = producto.cantidad * producto.precio;
    document.querySelector(`#importe${producto.id}`).innerHTML = new Intl.NumberFormat('es-AR').format(producto_importe);
    totalCarrito += producto.precio;
    console.log("totalCarrito: ", totalCarrito);
    document.querySelector('#tc').innerHTML = new Intl.NumberFormat('es-AR').format(totalCarrito);
    contador++;
    document.querySelector('#contador').innerHTML = contador;
    localStorage.setItem("carritoDulceMorena", JSON.stringify(carrito));

}
function resto(id){
    let producto = carrito.find(element => element.id === id);
    let posicion = carrito.indexOf(producto);
    producto.cantidad--;
    carrito.splice(posicion, 1, producto);
    document.querySelector(`#canti${producto.id}`).innerHTML = producto.cantidad;
    let producto_importe = producto.cantidad * producto.precio;
    document.querySelector(`#importe${producto.id}`).innerHTML = new Intl.NumberFormat('es-AR').format(producto_importe);
    totalCarrito -= producto.precio;
    console.log("totalCarrito: ", totalCarrito);
    document.querySelector('#tc').innerHTML = new Intl.NumberFormat('es-AR').format(totalCarrito);
    contador--;
    document.querySelector('#contador').innerHTML = contador;

    if (producto.cantidad === 0) {
        carrito.splice(posicion, 1);    //ELIMINA EL ELEMENTO DE CARRITO Y LUEGO ARMA EL MODAL DE NUEVO
        armaModal();
    }
    localStorage.setItem("carritoDulceMorena", JSON.stringify(carrito));
    if (carrito.length === 0) localStorage.removeItem('carritoDulceMorena');
}
function verifyLocalStorageCarrito(){
    console.log("Entré a verify")
    if (localStorage.getItem("carritoDulceMorena")) {
        //creamos el item de localstorage
        carrito = JSON.parse(localStorage.getItem("carritoDulceMorena"));
        contador = 0;
        carrito.forEach((element, indice) =>{
            contador += element.cantidad;
            console.log('element.id del carrito: ', element.id);
            fetch(`/productos/${element.id}`, { method: 'GET'})
        .then(response => response.json())
        .then(data => {
            let producto = data [0];
            console.log("Producto recibido del fetch ", producto);
            console.log('Carrito en el fetch: ', carrito);
            console.log('element.precio en el carrito: ', element.precio);
            console.log('producto.precio de la base de datos: ',producto.precio);
            element.precio = producto.precio;
            
        });
        });
        localStorage.setItem("carritoDulceMorena", JSON.stringify(carrito));
        document.querySelector('#contador').innerHTML = contador;
    }
}
//funcion autoejecutable, que puede ser anonima o no 
//funciones autoejecutables javascript http://developer.mozilla,org/es/docs/Glossary/IIFE
(function main() {
    traeProductos();
    verifyLocalStorageCarrito()
})();


document.querySelector("#ingresaFace").setAttribute("onclick", "ingresaFace()");
document.querySelector("#ingresaInsta").setAttribute("onclick", "ingresaInsta()");
function ingresaFace() {
    location.href = "https://www.facebook.com/moredulces";  
}

function ingresaInsta() {
    location.href = "https://www.instagram.com/dulces.morena/?hl=es-la";  
}






